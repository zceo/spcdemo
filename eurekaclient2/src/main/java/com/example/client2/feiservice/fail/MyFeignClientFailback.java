package com.example.client2.feiservice.fail;

import com.example.client2.feiservice.MyFeignClient;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MyFeignClientFailback implements MyFeignClient {
    @Override
    public String hello() {
        return "hello callback";
    }

    @Override
    public String get1(int id, String name) {
        return id+":"+name+":callback";
    }

    @Override
    public String get2(Map<String, Object> map) {
        return "map callback";
    }
}
