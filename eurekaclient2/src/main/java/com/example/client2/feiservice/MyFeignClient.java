package com.example.client2.feiservice;

import com.example.client2.feiservice.fail.MyFeignClientFailback;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "provider", fallback = MyFeignClientFailback.class)  //name为远程服务名
public interface MyFeignClient {

    @LoadBalanced
    @RequestMapping(value = "/hello")
    public String hello();

    @RequestMapping("/get1")
    public String get1(@RequestParam("id") int id, @RequestParam("name") String name);

    @RequestMapping("/get2")
    public String get2(@RequestParam Map<String, Object> map);
}
