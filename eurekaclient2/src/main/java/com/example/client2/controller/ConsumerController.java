package com.example.client2.controller;

import com.example.client2.feiservice.MyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ConsumerController {

    @Autowired
    private MyFeignClient myFeignClient;

    @RequestMapping("/index")
    public String index(){
        return  myFeignClient.hello();
    }

    @RequestMapping("/getone")
    public String getOne(@RequestParam("id") int id, @RequestParam("name") String name){
        return  myFeignClient.get1(id, name);
    }

    @RequestMapping("/gettwo")
    public String getTwo(@RequestParam("id") int id, @RequestParam("name") String name){
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("name", name);

        return  myFeignClient.get2(map);
    }
}
