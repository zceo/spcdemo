package com.example.client1.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HelloController {

    @Value("${provider.name}")
    private String name;

    @Value("${server.port}")
    private String port;

    @RequestMapping("/hello")
    public String hello(){
        String str = "Hello "+name+":"+port;
        return str;
    }

    @RequestMapping("/get1")
    public String get1(@RequestParam("id") int id, @RequestParam("name") String name) throws InterruptedException{
        String str = "Hello "+name+":"+port+"/id="+id+";name="+name;
        Thread.sleep(3000);
        return str;
    }

    @RequestMapping("/get2")
    public String get2(@RequestParam Map<String, Object> map){

        String str = "Hello "+name+":"+port+"/map:id="+map.get("id")+";name="+map.get("name");
        return str;
    }

    @RequestMapping("/get3")
    public String get3(){

    }

    public void listNodeDelete(Node head) {
        Node nHead = head;
        Node nNext = nHead.next;

        if (nNext == null || nHead.t == nNext.t) {
            nHead = nNext.next;
        }

        int cat;
        do{
            cat = 0;
            if(nHead.t == head.t) cat++;
            Node next = head.next;
            while (next != null) {
                if (next.t == nHead.t) {
                    cat++;
                } else {
                    next = next.next;
                }
            }
            if(cat > 1) nHead = nHead.next;
        }while (cat > 1 );


    }


    public class Node<T>{
        private T t;
        private Node next;
        public Node(T t,Node next){
            this.t = t;
            this.next = next;
        }
        public Node(T t){
            this(t,null);
        }
    }

    }

}



